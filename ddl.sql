CREATE TABLE `admin` (
    id_admin INT PRIMARY KEY NOT NULL,
    email VARCHAR(60),
    password VARCHAR (10),
    nama VARCHAR(60),
    no_telp VARCHAR(15),
    jenis_kelamin CHAR(1),
    is_customer_service CHAR(1)
);

CREATE TABLE `customer` (
    id_customer INT PRIMARY KEY NOT NULL,
    email VARCHAR(60),
    password VARCHAR (10),
    nama VARCHAR(100),
    no_telp VARCHAR(15),
    jenis_kelamin CHAR(1)
);

CREATE TABLE `restoran` (
    id_resto INT PRIMARY KEY NOT NULL,
    nama VARCHAR(50),
    alamat VARCHAR (60),
    no_telp VARCHAR(15),
    promo VARCHAR(50),
    kategori VARCHAR(50)
);

CREATE TABLE `sistem` (
    id_sistem INT PRIMARY KEY NOT NULL,
    id_admin INT NOT NULL REFERENCES `admin` (id_admin),
    id_customer INT NOT NULL REFERENCES `customer` (id_customer),
    id_resto INT NOT NULL REFERENCES `restoran` (id_resto)
);