<html>
    <head>
    <meta charset="UTF-8">
        <title>FOODREV</title><head>
        <link href="<?php echo base_url('css/normalize.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/main.css') ?>" rel="stylesheet" media="screen" type="text/css">
        <link href="<?php echo base_url('http://fonts.googleapis.com/css?family=Pacifico') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('http://fonts.googleapis.com/css?family=Playball') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('css/bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/style-portfolio.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/picto-foundry-food.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/jquery-ui.css') ?>" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0') ?>">
        <link href="<?php echo base_url('css/font-awesome.min.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('favicon-1.ico') ?>" rel="icon" type="image/x-icon">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">FOODREV</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav main-nav  clear navbar-right ">
                            <li><a class="navactive color_animation" href="#top">WELCOME</a></li>
                            <li><a class="color_animation" href="#story">ABOUT</a></li>
                            <li><a class="color_animation" href="#pricing">PRICING</a></li>
                            <li><a class="color_animation" href="#featured">FEATURED</a></li>
                            <li><a class="color_animation" href="#contact">CONTACT</a></li>
                            <li><a class="color_animation" href="<?php echo base_url()?>">LOGOUT</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>
         
        <div id="top" class="starter_container bg">
            <div class="follow_container">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="top-title"> FOODREV</h2>
                    <h2 class="white second-title">" Best guide to a great food "</h2>
                    <hr>
                </div>
            </div>
        </div>

        <!-- ============ About Us ============= -->

        <section id="story" class="description_content">
            <div class="text-content container">
                <div class="col-md-6">
                    <h1>About us</h1>
                    <div class="fa fa-cutlery fa-2x"></div>
                    <p class="desc-text">FOODREV merupakan sebuah platform yang berfungsi sebagai Restaurant Search and Discovery Service untuk menjabarkan pilihan tempat makan yang terbaik. 
                    Dan juga menjadi Tugas 2 untuk mata kuliah Pemrograman Aplikasi Berbasis Web (PABW) untuk Alya, Annisa, Haris, Junjungan dan Nella</p>
                </div>
                <div class="col-md-6">
                    <div class="img-section">
                       <img src="http://localhost/pabw/images/kabob.jpg" width="250" height="220">
                       <img src="http://localhost/pabw/images/limes.jpg" width="250" height="220">
                       <div class="img-section-space"></div>
                       <img src="http://localhost/pabw/images/radish.jpg" width="250" height="220">
                       <img src="http://localhost/pabw/images/corn.jpg" width="250" height="220">
                   </div>
                </div>
            </div>
        </section>


       <!-- ============ Pricing  ============= -->


        <section id="pricing" class="description_content">
             <div class="pricing background_content">
                <h1><span>Affordable</span> Restaurant</h1>
             </div>
            <div class="text-content container"> 
                <div class="container">
                    <div class="row">
                        <div id="w">
                            <ul id="filter-list" class="clearfix">
                                <li class="filter" data-filter="all">All</li>
                                <li class="filter" data-filter="breakfast">Breakfast</li>
                                <li class="filter" data-filter="special">Special</li>
                                <li class="filter" data-filter="desert">Desert</li>
                                <li class="filter" data-filter="dinner">Dinner</li>
                            </ul><!-- @end #filter-list -->    
                            <ul id="portfolio">
                                <li class="item breakfast">
                                    <img src="http://localhost/pabw/images/food_icon01.jpg" alt="Food">
                                    <h2 class="white">Maple and Oak</h2>
                                    <p class="black">Diskon 20%</p>
                                </li>
                                <li class="item dinner special">
                                    <img src="http://localhost/pabw/images/food_icon02.jpg" alt="Food">
                                    <h2 class="white">Sophie Authentique</h2>
                                    <p class="black">Diskon 15%</p>
                                </li>
                                <li class="item dinner breakfast">
                                    <img src="http://localhost/pabw/images/food_icon03.jpg" alt="Food">
                                    <h2 class="white">Astoria Cafe</h2>
                                    <p class="black">Diskon 5%</p>
                                </li>
                                <li class="item special">
                                    <img src="http://localhost/pabw/images/food_icon04.jpg" alt="Food">
                                    <h2 class="white">Hooters Jakarta</h2>
                                    <p class="black">Diskon 30%</p>
                                </li>
                                <li class="item dinner">
                                    <img src="http://localhost/pabw/images/food_icon05.jpg" alt="Food">
                                    <h2 class="white">Gordi Arkadia</h2>
                                    <p class="black">Diskon 25%</p>
                                </li>
                                <li class="item special">
                                    <img src="http://localhost/pabw/images/food_icon06.jpg" alt="Food">
                                    <h2 class="white">MINQ Kitchen</h2>
                                    <p class="black">Diskon 15%</p>
                                </li>
                                <li class="item desert">
                                    <img src="http://localhost/pabw/images/food_icon07.jpg" alt="Food">
                                    <h2 class="white">Gia Restaurant</h2>
                                    <p class="black">Diskon 50%</p>
                                </li>
                                <li class="item desert breakfast">
                                    <img src="http://localhost/pabw/images/food_icon08.jpg" alt="Food">
                                    <h2 class="white">Social House</h2>
                                    <p class="black">Diskon 40%</p>
                                </li>
                            </ul><!-- @end #portfolio -->
                        </div><!-- @end #w -->
                    </div>
                </div>
            </div>  
        </section>

        <!-- ============ Featured Restaurant  ============= -->

        <section id="featured" class="description_content">
            <div class="featured background_content">
                <h1>Our Featured  <span>Restaurant</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>Bleu Alley Brasserie</h1>
                    <div class="icon-hotdog fa-2x"></div>
                    <p class="desc-text">Merupakan restoran fancy yang hadir di Kelapa Gading. Menonjolkan konsep restoran khas Prancis, Bleu Alley Brasserie bisa jadi pilihan kamu jika ingin makan sembari menyesap satu atau dua gelas wine bersama orang-orang terkasih. Suasananya yang agak remang menciptakan kesan mewah dan romantis! </p>
                </div>
                <div class="col-md-6">
                    <ul class="image_box_story2">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item">
                                    <img src="http://localhost/pabw/images/slider1.jpg" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="http://localhost/pabw/images/slider2.jpg" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item active">
                                    <img src="http://localhost/pabw/images/slider3.jpg" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </section>


        <!-- ============ Social Section  ============= -->
      
        <section class="social_connect">
            <div class="text-content container"> 
                <div class="col-md-6">
                    <span class="social_heading">FOLLOW</span>
                    <ul class="social_icons">
                        <li><a class="icon-twitter color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-github color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-linkedin color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-mail color_animation" href="#"></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class="social_heading">OR DIAL</span>
                    <span class="social_info"><a class="color_animation" href="tel:82413419">(021) 82413419</a></span>
                </div>
            </div>
        </section>

        <!-- ============ Contact Section  ============= -->

        <section id="contact">
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3618.664063989472!2d91.8316103150038!3d24.909437984030877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37505558dd0be6a1%3A0x65c7e47c94b6dc45!2sTechnext!5e0!3m2!1sen!2sbd!4v1444461079802" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
            </div>
        </section>

        <script src="<?php echo base_url('js/jquery-1.10.2.min.js')?>"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('js/jquery-1.10.2.js')?>"></script>
        <script src="<?php echo base_url('js/jquery.mixitup.min.js')?>"></script>
        <script src="<?php echo base_url('js/main.js')?>"></script>

    
</body>
</html>
